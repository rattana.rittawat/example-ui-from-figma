package com.example.examplesilentmoon.ui.signin

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.examplesilentmoon.R
import com.example.examplesilentmoon.utils.getUnderLineColorText
import kotlinx.android.synthetic.main.sign_in_fragment.*

class SignInFragment : Fragment() {

    companion object {
        fun newInstance() = SignInFragment()
    }

    private lateinit var viewModel: SignInViewModel
    private var navController: NavController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sign_in_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SignInViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        setUpdateUi()
    }

    private fun setUpdateUi() {
        val string = "ALREADY HAVE AN ACCOUNT? SIGN UP";
        val targetString = "SIGN UP";
        tvAccountSignUp.text = getUnderLineColorText(string, targetString, resources.getColor(R.color.purple_700))

        tvAccountSignUp.setOnClickListener {
            navController?.navigate(SignInFragmentDirections.actionSignInFragmentToSignUpFragment())
        }

        btnBackPage.setOnClickListener {
            navController?.popBackStack()
        }

    }

}