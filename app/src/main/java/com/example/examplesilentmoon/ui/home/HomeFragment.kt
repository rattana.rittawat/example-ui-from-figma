package com.example.examplesilentmoon.ui.home

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.compose.navArgument
import androidx.navigation.fragment.findNavController
import com.example.examplesilentmoon.R
import com.example.examplesilentmoon.utils.getUnderLineColorText
import kotlinx.android.synthetic.main.home_fragment.*

class HomeFragment : Fragment() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    private lateinit var viewModel: HomeViewModel
    private var navController: NavController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        setUpdateUi()
    }

    private fun setUpdateUi() {
        val string = "ALREADY HAVE AN ACCOUNT? LOG IN";
        val targetString = "LOG IN";
        tvAccountLogin.text = getUnderLineColorText(string, targetString, resources.getColor(R.color.purple_700))
        tvAccountLogin.setOnClickListener {
            navController?.navigate(HomeFragmentDirections.actionHomeFragmentToSignInFragment())
        }
    }

}