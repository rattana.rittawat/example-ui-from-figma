package com.example.examplesilentmoon.ui.signup

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.examplesilentmoon.R
import com.example.examplesilentmoon.ui.signin.SignInFragmentDirections
import com.example.examplesilentmoon.utils.getUnderLineColorText
import kotlinx.android.synthetic.main.sign_in_fragment.*
import kotlinx.android.synthetic.main.sign_up_fragment.*

class SignUpFragment : Fragment() {

    companion object {
        fun newInstance() = SignUpFragment()
    }

    private lateinit var viewModel: SignUpViewModel
    private var navController: NavController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sign_up_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SignUpViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        setUpdateUi()
    }

    private fun setUpdateUi() {
        val string = "i have read the Privacy Policy";
        val targetString = "Privacy Policy";
        cbPolicy.text = getUnderLineColorText(string, targetString, resources.getColor(R.color.purple_700))

        btnSignUp.setOnClickListener {
            navController?.navigate(SignUpFragmentDirections.actionSignUpFragmentToWelcomeFragment())
        }
    }

}