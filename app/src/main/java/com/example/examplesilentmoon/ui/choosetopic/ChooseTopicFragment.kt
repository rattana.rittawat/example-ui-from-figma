package com.example.examplesilentmoon.ui.choosetopic

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.compose.navArgument
import androidx.navigation.fragment.findNavController
import com.example.examplesilentmoon.R

class ChooseTopicFragment : Fragment() {

    companion object {
        fun newInstance() = ChooseTopicFragment()
    }

    private lateinit var viewModel: ChooseTopicViewModel
    private var navController: NavController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.choose_topic_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ChooseTopicViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()

        setUpdateUi()
    }

    private fun setUpdateUi() {

    }

}