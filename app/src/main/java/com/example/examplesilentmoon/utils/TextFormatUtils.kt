package com.example.examplesilentmoon.utils

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan

fun getUnderLineColorText(str: String, targetString: String, color: Int) : SpannableString {
    val spannableString = SpannableString(str)
    val targetStartIndex = str.indexOf(targetString)
    val targetEndIndex = targetStartIndex + targetString.length
    spannableString.setSpan(ForegroundColorSpan(color), targetStartIndex, targetEndIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    spannableString.setSpan(UnderlineSpan(), targetStartIndex, targetEndIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

    return spannableString
}

